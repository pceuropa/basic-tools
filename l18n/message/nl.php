<?php

$m = [
'title' => 'Het maken van websites, online winkels, logo, blog PCEUROPA Webservices',
'description' => 'PCEUROPA Webservices : Het maken van websites | E-commerce | Logo | online winkels | Blogs ',


'Home' => 'Home',
'Offer' => 'Aanbod',
'Creating websites' => 'Het maken van websites',
'home_1' => 'PCEUROPA Ltd. komt uw wensen tegemoet. Wij zijn een team gekwalificeerde en creatieve mensen die voor elk 
aspect van uw project zal zorgen, zowel qua programmering als in visueel opzicht.',

	
'offre_1' => 'Het maken van websites',
'offre_2' => 'Het ontwerpen van online winkels',
'offre_3' => 'Websites voor wetenschappelijk onderzoek',
'offre_4' => 'Merkimago - huisstijl, logo, briefpapier',
'offre_5' => 'Opgedragen portals, social networking diensten',
'offre_6' => 'Internet-toepassingen, mobiele',

'offre_1_more' => 'Een professionele website is het belangrijkste visitekaartje van een bedrijf en vrijwel de meest aantrekkelijke vorm van promotie binnen dit populaire medium - internet. Wij weten maar al te goed dat een website in elk opzicht uniek dient te zijn om aan uw verwachtingen te voldoen, oftewel de gewenste voordelen te geven, zoals het creëren van een positief bedrijfsimago en het aantrekken van veel nieuwe klanten. ',

'knowledge_web_title' => 'Kennis van de techniek gebruikt op het internet',
'knowledge' => 'niveau van kennis',

//portfolio
'alt_1' => 'het creëren van logo voor bedrijven',
'alt_2' => 'webdesign',
'alt_3' => 'het creëren van weblogs',
'alt_4' => 'website gedaan in html5',
'alt_5' => 'moderne websites',
'alt_6' => 'website op het tablet en smartphone',
'alt_5' => 'responsieve webpagina',

'Web Tech' => 'Technologieën',
'Science Tech' => 'Wetenschappelijke Technologies',
'Created' => 'Gemaakt',

//contact
'name' => '',
'contac_human' => "",
'Submit' => "Sturen",


//contact
'contact_name' => 'Voornaam en achternaam',
'contact_email' => 'Email',
'contact_attention' => 'Bericht',
'contact_human' => 'Selecteer om te sturen',
'contact_submit' => 'Sturen',
'contact_php_back' => "<a href='./#contact'>Terug naar vormen</a>",

/* Walidacja Jquery */
'messages_name' => 'Please enter a name',
'messages_name_minlength' => 'Enter although the two characters',
'messages_name_maxlength' => 'You can only enter 90 characters',
'messages_attention' => 'Please enter a message',
'messages_attention_minlength' => 'Please write a minimum of 20 characters',
'messages_attention_maxlength' => 'The message can not be longer than 2000 characters',
'messages_email' => 'Please enter a valid email address',
'messages_human' => 'Approval of this field is required',
'messages_answer' => 'Selecteer om te sturen',
'messages_shortage' => 'lack of data needed to dispatch messages Email',
];


?>