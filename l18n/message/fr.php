<?php
$m = [
'title' => 'Création de sites web boutiques en ligne logo PCEUROPA Webservices',
'description' => 'PCEUROPA Webservices : Création de sites web | E-commerce | Logo | boutiques en ligne | Blogs ',

'Home' => 'Accueil',
'Offer' => 'Offre',
'Creating websites' => 'Création des sites Web',
'home_1' => 'PCEUROPA Ltd répond à vos attentes. Nous sommes une équipe de personnes qualifiées et créatives qui 
	prendront soin de chaque aspect de votre projet - qu\'il s\'agisse de la programmation ou de la visualisation.',
	
'offre_1' => 'Création de sites web',
'offre_2' => 'Création de boutiques en ligne | E-commerce',
'offre_3' => 'Sites Web pour la recherche scientifique',
'offre_4' => ' (Boutiques en ligne)',
'offre_4' => 'L\'image de marque - des sociétés identité, logo, papier à lettres, flyers',
'offre_5' => 'Portails dédiés, industrie, le réseautage social',
'offre_6' => 'Applications Internet, mobiles',

'offre_1_more' => 'Le site internet professionnel est une véritable carte de visite et probablement la forme de promotion la plus efficace offerte par le médium le plus populaire - l\'internet. Nous sommes conscients que le site internet devrait être exceptionnel et unique pour satisfaire à vos exigences et pour assurer les avantages souhaités comme une image positive de votre entreprise et de nouveaux clients.',

'knowledge_web_title' => 'La connaissance de la technologie utilisée sur l\'Internet',
'knowledge' => 'niveau de connaissance',

//portfolio
'alt_1' => 'création de logos pour les entreprises',
'alt_2' => 'conception de sites Web',
'alt_3' => 'créer des weblogs',
'alt_4' => 'site fait en html5s',
'alt_5' => 'sites modernes',
'alt_6' => 'site sur la tablette et smartphone',
'alt_5' => 'page Web responsive',

'Web Tech' => 'Technologies',
'Science Tech' => 'Scientific Technologies',
'Created' => 'Créé en',

//contact
'contact_name' => 'Prénom et nom',
'contact_email' => 'Email',
'contact_attention' => 'Message',
'contact_human' => 'Cochez cette case si vous êtes un homme.',
'contact_submit' => 'Envoyer',
'contact_php_back' => "<a href='./#contact'>Revenir au formulaire</a>",

// Walidacja Jquery 

'messages_name' => 'Entrez votre un nom',
'messages_name_minlength' => 'Écrire un minimum de 2 caractères',
'messages_name_maxlength' => 'Écrire un maximum de 90 caractères',
'messages_attention' => 'S\'il vous plaît entrer un message',
'messages_attention_minlength' => 'Écrire un minimum de 20 caractères',
'messages_attention_maxlength' => 'Écrire un maximum de 2000 caractères',
'messages_email' => 'Entrez votre adresse e-mail',
'messages_human' => 'Approbation est nécessaire',
'messages_answer' => 'Merci, bientôt à répondre à votre message',
'messages_shortage' => 'manque de données nécessaires pour distribuer des messages e-mail',


];

?>