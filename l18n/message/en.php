<?php

$m = [
'title' => 'Creating websites www online stores logo blogs PCEUROPA Webservices',
'description' => 'PCEUROPA Webservices : Creating Websites | E-commerce | Logo | online stores | Blogs ',



'Home' => 'Accueil',
'Offer' => 'Offre',
'Creating websites' => 'Création des sites Web',
'home_1' => 'PCEUROPA Ltd. wants to meet your expectations. We are a group of qualified and creative people who will take care of each 
and every aspect of your design, both programming and visualization.',

//offre
'offre_1' => 'Web Design & creations',
'offre_2' => 'Designing online stores',
'offre_3' => 'Websites for scientific research',
'offre_4' => 'Brand image - corporate identity, logo, letterhead',
'offre_5' => 'Dedicated portals, social networking services',
'offre_6' => 'Internet applications, mobile',

'offre_1_more' => 'A professional website is the main showcase of the company and the most attractive form of promotion in the most popular medium - the Internet. We know perfectly that a website should be unique in each respect if it is to meet your expectations, i.e. to bring the desired benefits, which include creation of a positive company image as well as attracting crowds of new customers.',
'knowledge_web_title' => 'Knowledge of the technology used on the Internet',
'knowledge' => 'level of knowledge',


//portfolio
'alt_1' => 'creating logos for companies',
'alt_2' => 'web design belgium',
'alt_3' => 'creating weblogs',
'alt_4' => 'website done in html5',
'alt_5' => 'modern websites',
'alt_6' => 'website on the tablet and smartphone',
'alt_5' => 'responsive web page',


//contact

'contact_name' => 'First name and last name',
'contact_email' => 'Email Address',
'contact_attention' => 'Content',
'contact_human' => 'Select if you are a human.',
'contact_submit' => 'Submit',
'contact_php_back' => "Mark you're a human <a href='javascript:history.go(-1);'>Return to form</a>",

/* Walidacja Jquery */
'messages_name' => 'Please enter a name',
'messages_name_minlength' => 'Enter although the two characters',
'messages_name_maxlength' => 'You can only enter 90 characters',
'messages_attention' => 'Please enter a message',
'messages_attention_minlength' => 'Please write a minimum of 20 characters',
'messages_attention_maxlength' => 'The message can not be longer than 2000 characters',
'messages_email' => 'Please enter a valid email address',
'messages_human' => 'Approval of this field is required',
'messages_answer' => 'Thank you, soon to answer your message',
'messages_shortage' => 'lack of data needed to dispatch messages Email',

];





?>