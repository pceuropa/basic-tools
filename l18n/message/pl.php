<?php

$m = [
'title' => 'Tworzenie stron internetowych Belgia Francja Holandia PCEUROPA Webservices',
'description' => 'PCEUROPA Webservices : Tworzenie stron internetowych sklepów | E-commerce | Logo | Blogów w Belgii Francji Holandii i Hiszpanii',



'Home' => 'Główna',
'Offer' => 'Oferta',
'Contact' => 'Kontakt',
'Creating websites' => 'Tworzenie stron internetowych',
'home_1' => 'PCEUROPA Ltd. wychodzi naprzeciw waszym pragnieniom. Jesteśmy grupą wykwalifikowanych i kreatywnych osób, 
które zadbają o każdy aspekt Waszego projektu, zarówno programistyczny, jak i wizualny.',

	//offre
'offre_1' => 'Tworzenie stron internetowych',
'offre_2' => 'Projektowanie sklepów internetowych',
'offre_3' => 'Strony na potrzeby badań naukowych',
'offre_4' => 'Wizerunek marki - identyfikacja wizualna, logo, papier firmowy, ulotki',
'offre_5' => 'Portails dédiés, industrie, social',
'offre_6' => 'Aplikacje internetowe, mobilne',

'offre_1_more' => 'Profesjonalna strona internetowa jest wizytówką firmy i bardzo atrakcyjną formą promocji 
w najpopularniejszym obecnie medium, którym jest Internet. Doskonale wiemy, ze strona internetowa powinna być wyjątkowa 
pod każdym względem jeśli ma spełnić Wasze oczekiwania, czyli przynieść upragnione korzyści, jakimi są wykreowanie 
pozytywnego wizerunku przedsiębiorstwa i zachowywanie starych klientów . ',

'knowledge_web_title' => 'Znajomość technologii używanych w internecie',
'knowledge' => 'poziom wiedzy',

//portfolio
'alt_1' => 'tworzenie logo dla firm',
'alt_2' => 'tworzenie stron www',
'alt_3' => 'tworzenie blogów internetowych ',
'alt_4' => 'strony www zrobione w html5',
'alt_5' => 'nowoczesne strony internetowe',
'alt_6' => 'strony www na tablet i smartfon',
'alt_5' => 'responsywne strony web',
'Web Tech' => 'Technologie',
'Science Tech' => 'Technologie Naukowe',
'Created' => 'Stworzono',

//contact
'contact_name' => 'Imię i nazwisko',
'contact_email' => 'Email',
'contact_attention' => 'Treść',
'contact_human' => 'Zaznacz jeżeli jesteś człowiekiem',
'contact_submit' => 'Wyślij',
'contact_php_back' => "<a href='./#contact'>Wróć do formularza</a>",

// Walidacja Jquery 
'messages_name' => 'Proszę podać imię i nazwisko',
'messages_name_minlength' => 'Wpisz chociaż 2 znaki',
'messages_name_maxlength' => 'Można wpisać tylko 90 znaków',
'messages_attention' => 'Proszę wpisać treść wiadomości',
'messages_attention_minlength' => 'Wpisz pisać minimum 20 znaków',
'messages_attention_maxlength' => 'Wiadomość nie może mieć więcej niż 2000 znaków',
'messages_email' => 'Proszę poprawny podać adres email',
'messages_human' => 'Zatwierdzenie tego pola jest wymagane',
'messages_answer' => 'Dziękujemy, Niebawem odpowiemy na twoją wiadomość',
'messages_shortage' => 'brak potrzebnych danych do wysyłki wiadomości Email'
];



?>