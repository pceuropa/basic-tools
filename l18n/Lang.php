<?php namespace pceuropa\l18n;

class Lang {

static public $array_words;
private $iso = 'en';
function __construct() {
	
	if (array_key_exists('language', func_get_args()[0])) {
		$this->iso = func_get_args()[0]['language'];
	}
	
	switch (true){
	case (isset($this->iso)): {include "message/$this->iso.php";} break;
	case (isset($_GET['lang'])): {include "message/".$_GET['lang'].".php";} break;
	default: { include "message/en.php";}
	}
	
	self::$array_words = $m;

}

public function IsoLang(){

	if(isset($_GET['lang']))
	{
		return $this->iso = $_GET['lang'];
	}
	 

}


public static function t($param){

	if (array_key_exists($param, self::$array_words)){

		echo self::$array_words[$param];
		} else {
		echo $param;}

}

}

//$o = new Lang;
//echo $o->t('Home');
//echo Lang::t('Home');